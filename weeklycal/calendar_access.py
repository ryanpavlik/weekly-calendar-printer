# Copyright 2021-2022, Collabora, Ltd.
#
# SPDX-License-Identifier: MIT
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>

import re
from typing import Generator, List, Optional

import caldav
import click
from caldav.objects import Event
from dateutil import tz
from vobject.base import ParseError

from weeklycal.datatypes import CalendarEvent

_BAD_DTSTART_RE = re.compile(
    r"""BEGIN:STANDARD
TZNAME:LMT
TZOFFSETFROM:[-+]?[0-9]{6}
TZOFFSETTO:[-+]?[0-9]{6}
DTSTART:00000000T000000
END:STANDARD
""",
    flags=re.MULTILINE,
)


def _sanitize_event(event: Event):
    """Fix weird things that evolution sometimes does that breaks parsing."""
    match = _BAD_DTSTART_RE.search(event.data)
    if not match:
        return False

    event.data = _BAD_DTSTART_RE.sub("", event.data)
    return True


def _get_calendar_events_ical(
    server, username, password, calendar_name, start_date, end_date
) -> List[caldav.Event]:
    client = caldav.DAVClient(url=server, username=username, password=password)
    principal = client.principal()
    cals = principal.calendars()
    calendar: Optional[caldav.Calendar] = None
    if calendar_name:
        filtered = [cal for cal in cals if cal.name == calendar_name]
        if not filtered:
            click.echo(f"Could not find calendar named {calendar_name}", err=True)
            calendar_name = None
        else:
            calendar = filtered[0]

    if not calendar:
        if len(cals) == 1:
            click.echo(
                f"No valid calendar name provided, but only one available: {cals[0].name} at {cals[0].url}"
            )
            calendar = cals[0]
        else:
            click.echo("No valid calendar name provided, and more than one available.")
            for cal in cals:
                click.echo(f"- {cal.name} - {cal.url}")
            raise click.ClickException("Must specify a valid calendar name")
    return calendar.date_search(start=start_date, end=end_date)


def generate_calendar_events(
    server, username, password, calendar_name, start_date, end_date
) -> Generator[CalendarEvent, None, None]:
    for event in _get_calendar_events_ical(
        server, username, password, calendar_name, start_date, end_date
    ):
        event.load()
        if _sanitize_event(event):
            print("Fixed invalid event, trying to save changes")
            event.save(no_create=True)
        try:
            vevent = event.vobject_instance.vevent  # type: ignore
        except ParseError as e:
            print("Skipping an event due to a parse error!", e)
            print(event.data)
            continue
        summary = vevent.summary.value
        # Might be a date or a datetime
        try:
            start = vevent.dtstart.value.astimezone(tz=tz.tzlocal())
        except AttributeError:
            print("Skipping what is likely an all day event:", summary)
            continue
        ret = CalendarEvent(summary=summary, start=start)

        if "dtend" in vevent.contents:
            ret.set_duration_from_end(vevent.dtend.value)

        if "duration" in vevent.contents:
            ret.duration = vevent.duration.value

        yield ret
