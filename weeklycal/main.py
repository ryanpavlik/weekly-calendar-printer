# Copyright 2021-2024, Collabora, Ltd.
#
# SPDX-License-Identifier: MIT
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Main entrypoint."""

import datetime

import click
import dotenv
import pytz

from .calendar_access import generate_calendar_events
from .passwords import HAVE_SECRETSTORAGE, get_password, store_password
from .svg import CalendarSvg

dotenv.load_dotenv()


def find_best_monday(date: datetime.datetime) -> datetime.datetime:
    start_date = date
    if date.weekday() > 5:
        # on weekends, choose the upcoming week instead.
        start_date = date + datetime.timedelta(days=7)
    preceding_days = [start_date - datetime.timedelta(days=days) for days in range(7)]
    monday = next((x for x in preceding_days if x.weekday() == 0), None)
    assert monday
    return monday


@click.command()
@click.argument(
    "fn",
    type=click.Path(file_okay=True, dir_okay=False, writable=True, readable=False),
)
@click.option(
    "--classification",
    default="Internal",
    show_default=True,
    help="A document classification label to include on the output.",
)
@click.option(
    "--date",
    type=click.DateTime(formats=["%Y-%m-%d"]),
    default=str(datetime.date.today()),
    help="The date to start on looking for a Monday.",
)
@click.option(
    "--server",
    type=str,
    help="CalDAV server",
    required=True,
    envvar="WEEKLYCALENDAR_SERVER",
    show_envvar=True,
)
@click.option(
    "--username",
    type=str,
    help="CalDAV username",
    required=True,
    envvar="WEEKLYCALENDAR_USERNAME",
    show_envvar=True,
)
@click.option(
    "--password",
    type=str,
    help="CalDAV password",
    envvar="WEEKLYCALENDAR_PASSWORD",
    show_envvar=True,
)
@click.option(
    "--calendar-name",
    "calendar_name",
    default=None,
    type=str,
    help="Calendar name. If not provided and if more than one is available, a list will be shown",
    envvar="WEEKLYCALENDAR_CALENDARNAME",
    show_envvar=True,
)
@click.option(
    "--event-titles/--no-event-titles",
    default=True,
    show_default=True,
    help="Whether to include event titles in blocks, or hide them instead for confidentiality.",
)
@click.option(
    "--use-secret-store/--skip-secret-store",
    default=True,
    show_default=True,
    help="Whether to use the platform secret store/keyring if available",
)
def cli(
    fn,
    date,
    classification,
    server,
    username,
    password,
    calendar_name,
    event_titles,
    use_secret_store,
):
    """
    Generate an SVG showing an hourly view of the work week, with events from a CalDAV server already included.

    If a .env file is available, it will be loaded to populate environment-variable-connected options.
    """
    loaded_password = None
    if use_secret_store and password is None:
        if HAVE_SECRETSTORAGE:
            loaded_password = get_password(server, username)
            if loaded_password is not None:
                click.echo("Password loaded from the secret service.")
                password = loaded_password
            else:
                click.echo("Could not load password.")
        else:
            click.echo("Secret storage package unavailable")
    if password is None:
        password = click.prompt(
            "Password for caldav server", hide_input=True, confirmation_prompt=False
        )

    if not date:
        date = datetime.datetime.now(datetime.UTC).replace(tzinfo=pytz.utc)
    monday = find_best_monday(date)

    friday = monday + datetime.timedelta(days=5)
    print(f"Using {monday.strftime('%Y-%m-%d')} - {friday.strftime('%Y-%m-%d')}")

    events_generator = generate_calendar_events(
        server, username, password, calendar_name, start_date=monday, end_date=friday
    )

    bg_svg = CalendarSvg.load(monday, classification=classification)
    for event in events_generator:
        if not event_titles:
            event.summary = ""

        duration = event.duration
        if not duration:
            print(f"Could not find duration for '{event.summary}', skipping!")
            continue
        print(
            f"Adding event '{event.summary}': starts {event.start}, lasts {event.duration}"
        )
        bg_svg.add_event(event.start, duration, event.summary)
    bg_svg.figure.save(fn)

    if use_secret_store and loaded_password != password:
        # Try storing it if we succeeded and got this far.
        if store_password(server, username, password):
            click.echo("Password saved to the secret service.")


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter,unexpected-keyword-arg
    cli(auto_envvar_prefix="WEEKLYCALENDAR")
