# Copyright 2021-2024, Collabora, Ltd.
#
# SPDX-License-Identifier: MIT
#
# Author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Eentrypoint for reduced features - no calendar access."""

import datetime

import click
import pytz

from .main import find_best_monday
from .svg import CalendarSvg


@click.command()
@click.argument(
    "fn",
    type=click.Path(file_okay=True, dir_okay=False, writable=True, readable=False),
)
@click.option(
    "--classification",
    default="Internal",
    show_default=True,
    help="A document classification label to include on the output.",
)
@click.option(
    "--date",
    type=click.DateTime(formats=["%Y-%m-%d"]),
    default=str(datetime.date.today()),
    help="The date to start on looking for a Monday.",
)
def cli(
    fn,
    classification,
    date,
):
    """Generate an SVG showing an hourly view of the work week, but with no events."""
    if not date:
        date = datetime.datetime.now(datetime.UTC).replace(tzinfo=pytz.utc)

    monday = find_best_monday(date)

    friday = monday + datetime.timedelta(days=5)
    print(f"Using {monday.strftime('%Y-%m-%d')} - {friday.strftime('%Y-%m-%d')}")

    bg_svg = CalendarSvg.load(monday, classification=classification)

    bg_svg.figure.save(fn)


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter,unexpected-keyword-arg
    cli(auto_envvar_prefix="WEEKLYCALENDAR")
